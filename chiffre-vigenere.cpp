//
// Created by antoine on 14/09/18.
//

#include <string>
#include <iostream>
#include "utils.h"

using namespace std;

int main(int argc, char *argv[]){
    string text = readInput(cin);
    string key = argv[1];
    //cout<< "text = " << text << endl << "key = " << key << endl;
    cout << chiffreVigenere(text, key) << endl;
}

std::string chiffreVigenere( const std::string & clair,
                             const std::string & cle ){

    string result = "";

    for(unsigned int i=0; i<clair.length(); i++){
        char keyReplacement = cle[i%(cle.length())];
        int shift = (keyReplacement - 'A') % 26;
        char shiftedValue = (((clair[i]  -'a') + shift) % 26) + 'a';
        //cout<< clair[i] << " shift : "<< shift << " with keyReplacement " << keyReplacement << "-> " << shiftedValue <<endl;
        result += shiftedValue + ('A'-'a');
    }

    return result;
}